
add_executable(generatorCellSorting EXCLUDE_FROM_ALL generator_cell_sorting.cpp)
InjectModels(generatorCellSorting)
target_link_libraries(generatorCellSorting ModelTesting)


set_property(DIRECTORY PROPERTY LABELS "Interaction" APPEND)

add_executable(testCellSorting test_cell_sorting.cpp)
InjectModels(testCellSorting)
target_link_libraries(testCellSorting PRIVATE ModelTesting gtest gtest_main)
gtest_discover_tests(testCellSorting WORKING_DIRECTORY "${PROJECT_DIR}")
