
add_executable(runInitTests const_initialization_test.cpp)
InjectModels(runInitTests)
target_link_libraries(runInitTests PRIVATE ModelTesting gtest gtest_main) # MorpheusCore

gtest_discover_tests( runInitTests WORKING_DIRECTORY "${PROJECT_DIR}")
