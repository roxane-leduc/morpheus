#include "model_test.h"
#include "core/simulation.h"
#include "core/celltype.h"
using namespace Eigen;
const double confidence = 0.95;

// Hex Stats
struct Data {double vol_mean; double vol_sigma; double surface_mean; double surface_sigma; };
std::vector<Data> hex_data =
{ { 1394.05, 3.31, 1090.95, 0.70 },
  { 1259.98, 6.06, 411.18, 0.33 },
  { 1253.81, 6.79, 293.15, 0.28 },
  { 1251.57, 7.00, 244.20, 0.26 },
  { 1267.34, 4.81, 520.21, 0.74 },
  { 1249.41, 5.99, 242.18, 0.38 },
  { 1242.65, 6.43, 194.29, 0.28 },
  { 1237.40, 6.07, 174.43, 0.21 },
  { 869.84, 17.98, 99.33, 0.98 },
  { 978.20, 11.77, 106.81, 0.56 },
  { 1031.30, 11.16, 110.87, 0.46 },
  { 1061.27, 8.35, 113.52, 0.30 },
  { 1.00, 0.01, 1.73, 0.01 },
  { 1.03, 0.22, 1.75, 0.11 },
  { 28.39, 26.99, 9.64, 6.64 },
  { 364.26, 18.04, 63.18, 1.51 }
};


std::vector<Data> square_data =
{ { 1323.42, 3.10, 938.72, 0.67 },
  { 1257.40, 4.52, 365.68, 0.29 },
  { 1255.08, 5.08, 266.49, 0.27 },
  { 1251.88, 4.78, 225.39, 0.21 },
  { 1258.74, 4.22, 365.77, 0.59 },
  { 1246.75, 5.64, 197.14, 0.30 },
  { 1233.98, 6.20, 168.23, 0.21 },
  { 1223.00, 5.38, 156.28, 0.22 },
  { 25.87, 32.68, 8.64, 8.18 },
  { 589.56, 13.09, 88.84, 0.97 },
  { 728.67, 9.62, 99.09, 0.64 },
  { 806.23, 9.21, 104.53, 0.55 },
  { 1.00, 0.01, 1.82, 0.01 },
  { 1.00, 0.01, 1.82, 0.01 },
  { 1.00, 0.01, 1.82, 0.01 },
  { 59.11, 2.64, 28.22, 0.60 },
};


TEST (CSMplane, Hexagonal) {
	
	auto file1 = ImportFile("csm_plane_hex.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(hex_data.size()*2);
	ArrayXd stddevs(hex_data.size()*2);
	int i=0;
	for (const auto& data : hex_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}

TEST (CSMplane, Square) {
	
	auto file1 = ImportFile("csm_plane_square.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(square_data.size()*2);
	ArrayXd stddevs(square_data.size()*2);
	int i=0;
	for (const auto& data : square_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}
