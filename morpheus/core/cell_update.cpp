#include "cell_update.h"
#include "celltype.h"

namespace CPM {
Update::Update(const UpdateData& data, shared_ptr<CPM::LAYER> layer) {
	d = new UpdateData_Private(data);
	d->ref_count=1;
	d->layer = layer;
	operation = NONE;
}

Update& Update::operator=(const Update& other) {
	if (d) d->ref_count--;
	if (d->ref_count ==0 ) delete d;
	d=other.d;
	d->ref_count++;
	operation = other.operation;
	return *this;
}
	
Update Update::clone() const {
	Update new_update(*this);
	new_update.d = new UpdateData_Private(*d);
	new_update.d->ref_count = 1;
// 	new_update.unset();
	
	return new_update;
}

void Update::set(VINT source, VINT dir, Update::Operation opx) {
	
	VINT pos = source;
	d->layer->lattice().resolve(pos);
	d->_source.setPosition(pos);
	
	pos+=dir;
	if ( ! d->layer->lattice().resolve(pos) )  {
		operation = NONE;
		return;
	}
	d->counter = update_counter();
	d->_focus.setPosition(pos);
	d->_focus_updated.setCell(d->_source.cellID(), pos);
	
	d->boundary.setPosition(focus().pos());
	d->update.setPosition(focus().pos());
	d->surface.setPosition(focus().pos());
	
	switch (opx) {
		case Operation::Extend :
			operation = ADD | REMOVE | NEIGHBORHOOD_UPDATE;
			break;
		case Operation::Move :
			operation = MOVE | NEIGHBORHOOD_UPDATE;
			break;
	}
}

void Update::set(VINT pos, CELL_ID cell_id) {
	
	if ( !d->layer->lattice().resolve(pos) ) {
		operation = NONE;
		return;
	}
	
	d->counter = update_counter();
	d->_source.unset();
	d->_focus.setPosition(pos);

	d->_focus_updated.setCell(cell_id,pos);
	
// 	if (focusUpdated().cell_index().status == SUPER_CELL) {
// 		// cannot add to supercell
// 		operation = NONE;
// 		return;
// 	}
	
	d->boundary.setPosition(focus().pos());
	d->update.setPosition(focus().pos());
	d->surface.setPosition(focus().pos());
	operation = ADD | REMOVE | NEIGHBORHOOD_UPDATE;
}


void Update::set(const UpdateSelection& selection, Update::Operation opx) {
	d->counter = update_counter();
	d->_source  = selection.source;
	d->_focus = selection.focus;
	d->_focus_updated.setCell(d->_source.cellID(), selection.focus.pos());
	
	d->boundary.setPosition(focus().pos());
	d->update.setPosition(focus().pos());
	d->surface.setPosition(focus().pos());
	
	switch (opx) {
		case Operation::Extend :
			operation = ADD | REMOVE | NEIGHBORHOOD_UPDATE;
			break;
		case Operation::Move :
			operation = MOVE | NEIGHBORHOOD_UPDATE;
			break;
	}
}

Update Update::selectOp(Update::AtomicOperation op) const
{
	auto sub_update = *this;
	sub_update.operation = (operation & op) ? op : NONE;
	return sub_update;
}

// const Update& Update::removeOp(Update::AtomicOperation op) const
// {
// 	operation_selected = operation_selected & (~op);
// 	return *this;
// }

// const Update& Update::resetOpSelection() const {
// 	operation_selected = operation;
// 	return *this;
// }

}
