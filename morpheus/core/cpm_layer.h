//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef LATTICE_STENCIL
#define LATTICE_STENCIL

#include "cpm_types.h"
#include "lattice_data_layer.h"

namespace CPM {
	ostream& operator <<(ostream& os, const CPM::STATE& n);         /// Output operator, exclusively used to save values of boundary conditions
	const int shadow_size = 3;
	typedef Lattice_Data_Layer<CPM::CELL_ID,shadow_size> LAYER;
	
}

/** @brief Extracts Information of a Neighborhood in terms of statistics in an efficient way.

 *  In particular it provides a CPM::CELL_ID - count statistics and the neighbor states, respecting boundary conditions.
 * 
 *  The stencil neighborhood available through getStencil() is in arbitrary order, sorted for optimized memory access patterns.
 */
class StatisticalLatticeStencil  {
	public:
		struct STATS { CPM::CELL_ID cell; uint count; };
		StatisticalLatticeStencil( shared_ptr< const CPM::LAYER > data_layer, const Neighborhood& neighbors );
		StatisticalLatticeStencil( const StatisticalLatticeStencil& other) = default;
		void setPosition(const VINT& pos);
		const Neighborhood& getStencil() const { return stencil; };
		const vector<StatisticalLatticeStencil::STATS>& getStatistics() const { if (!valid_data) applyPos(); return stencil_statistics; };
		const vector<CPM::CELL_ID>& getStates() const { if (!valid_data) applyPos(); return stencil_states; };
		
	private:
		void setStencil(const Neighborhood& neighbors);
		void applyPos() const;
		
		VINT pos;
		mutable bool valid_data;
		shared_ptr< const CPM::LAYER > data_layer;
		Neighborhood stencil;
		bool is_alternating;
		vector<int> stencil_offsets;
		vector<int> alternating_stencil_offsets;
// 		vector<int> stencil_row_offsets;
// 		vector<int> alternating_stencil_row_offsets;
		mutable vector <STATS> stencil_statistics;
		mutable vector<CPM::CELL_ID> stencil_states;
};

/** @brief Extracts Information of a Neighborhood in an efficient way.

 *  In particular it provides the neighborhood states, respecting boundary conditions.
 *
 *  The stencil neighborhood available through getStencil() is in exactly the order requested within the constructor.
 */

class LatticeStencil {
	public:
		
		LatticeStencil( shared_ptr< const CPM::LAYER > data_layer, const Neighborhood& neighbors );
		LatticeStencil( const LatticeStencil& other) = default;
		void setPosition(const VINT& pos);
		const Neighborhood& getStencil() const { return stencil; };
		const vector<CPM::CELL_ID>& getStates() const { if (!valid_data) applyPos(); return stencil_states; };
		
	private:
		void setStencil(const Neighborhood& neighbors);
		void applyPos() const;
		
		VINT pos;
		mutable bool valid_data;
		shared_ptr< const CPM::LAYER > data_layer;
		Neighborhood stencil;
		bool is_alternating;
		vector<int> stencil_offsets;
		vector<int> alternating_stencil_offsets;
		mutable vector<CPM::CELL_ID> stencil_states;
};


#endif //LATTICE_STENCIL

