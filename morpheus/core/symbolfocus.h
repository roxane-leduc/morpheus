//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef SYMBOLFOCUS_H
#define SYMBOLFOCUS_H
#include "cpm_types.h"

/** @brief Represents a spatial cursor for the retrieval of dependent symbolic information
// 	 *
// 	 * Resolving symbols during a simulation often demands to determine the context -- a cell or a position or whatsoever
// 	 * Finally, it is more efficient to just use a single object to fill that gap for all required symbols, which also slenderizes the interface for symbol retrieval.
// 	*/

enum class FocusRangeAxis { X, Y, Z, NODE, MEM_X, MEM_Y, MEM_NODE, CELL, CellType };

class SymbolFocus {
public:
	SymbolFocus() {};
	SymbolFocus(const VINT& pos);
	SymbolFocus(CPM::CELL_ID cell_id);
	SymbolFocus(CPM::CELL_ID cell_id, const VINT& pos);
	SymbolFocus(CPM::CELL_ID cell_id, double phi, double theta);
	/// Retrieve the cell at the current position
	const Cell& cell() const;
	uint celltype() const { return cell_index().celltype; };
	const string& celltype_name() const;
	/// Retrieve id of the cell at the current position
	const CPM::CELL_ID cellID() const;
	/// Retrieve position in lattice coordinates
	const VINT& pos() const;
	const VINT& cellPos() const;
	void updateCellPos(const VINT& cell_pos) const;
	/// Retrieve position in lattice coordinates
	const VINT& lattice_pos() const { return pos(); };
	/// Retrieve position in global coordinates, i.e. global scale and orthogonal coordinates
	const VDOUBLE& global_pos() const;
	/// Retrieve a membrane position in spherical coordinates. Throws an error if membrane position is not set. 
	const VINT& membrane_pos() const;
	int get(FocusRangeAxis axis) const;
	const CPM::INDEX& cell_index() const;
	void setCell(CPM::CELL_ID cell_id);
	void setCell(CPM::CELL_ID cell_id, const VINT& pos);
	void setPosition(const VINT& pos);
	bool hasPosition() const { return has_pos; }
	void setMembrane( CPM::CELL_ID cell_id, const VINT& pos );
	void unset();
	bool maybeUnderspecified() const { return maybe_underspecified; };
	void setUnderspecified(bool underspec = true) { maybe_underspecified = underspec; }
	bool valid() const { return has_pos || has_cell; }
	bool operator<(const SymbolFocus& rhs) const;
	bool operator==(const SymbolFocus& rhs) const;
	
	static const SymbolFocus global;
	
private:
	mutable bool has_pos=false, has_global_pos=false, has_cell_pos=false, has_membrane=false, has_cell=false, has_cell_index=false;
	bool maybe_underspecified = false;
	mutable VINT d_pos, d_cell_pos, d_membrane_pos;
	mutable VDOUBLE d_global_pos;
	mutable CPM::CELL_ID d_cell_id;
	mutable const Cell* d_cell=nullptr;
	mutable CPM::INDEX d_cell_index;
	
	friend ostream& operator<<(ostream& os, const SymbolFocus& n);
};

ostream& operator<<(ostream& os, const SymbolFocus& n);
#endif // SYMBOLFOCUS_H
